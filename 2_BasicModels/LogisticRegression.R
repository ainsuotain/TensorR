# Logistic Regresion using Tensorflow

library(magrittr)
library(data.table)
library(tensorflow)
library(keras)
use_python("/home/spark/hard/anaconda3/bin/python3")

tf$reset_default_graph()
# Import MNIST data using keras
mnist <- dataset_mnist()
TrainX <- mnist$train$x / 255.
TrainY <- mnist$train$y
TrainY_ <- as.data.table(TrainY)
setnames(TrainY_, "TrainY", "Y_")
TrainY_[, Y_ := as.factor(Y_)]
TrainY_ <- model.matrix(~Y_-1, TrainY_) %>% 
  as.data.table() %>%
  as.matrix()

TestX <- mnist$test$x / 255.
TestY <- mnist$test$y
TestY_ <- as.data.table(TestY)
setnames(TestY_, "TestY", "Y_")
TestY_[, Y_ := as.factor(Y_)]
TestY_ <- model.matrix(~Y_-1, TestY_) %>% 
  as.data.table() %>%
  as.matrix()

# 28X28 image to 1X784 vector
TrainX <- matrix(TrainX, nrow = dim(TrainX)[1])
TestX <- matrix(TestX, nrow = dim(TestX)[1])

# Parameters
learning_rate <- 0.01
training_epochs <- 25L
batch_size <- 100L
display_step <- 1L

# tf graph input
X <- tf$placeholder(tf$float32, shape = shape(NULL, 784L))
Y <- tf$placeholder(tf$float32, shape = shape(NULL, 10L))

# Set model weights
W <- tf$Variable(tf$zeros(shape(784L, 10L)))
b <- tf$Variable(tf$zeros(shape(10L)))

# Construct model
pred <- tf$nn$softmax(tf$matmul(X, W) + b)

# Minimize error using cross entropy
cost <- -tf$reduce_mean(tf$reduce_sum(Y * tf$log(pred + 1e-15), reduction_indices = 1L))
# Gradient Descent
optimizer <- tf$train$GradientDescentOptimizer(learning_rate)$minimize(cost)

# Initialize the variables (i.e. assign their default value)
init <- tf$global_variables_initializer()

# Start training
with(tf$Session() %as% sess, {
  
  # Run the initializer
  sess$run(init)
  
  # Training cycle
  for (epoch in 1:training_epochs){
    avg_cost <- 0.
    total_batch <- as.integer(nrow(TrainX) / batch_size)
    # Loop over all batches
    for (i in 1:total_batch){
      batch_idx <- ((i-1)*batch_size + 1):(i*batch_size)
      batch_xs <- TrainX[batch_idx,]
      batch_ys <- TrainY_[batch_idx,]
      # Run optimization op (backprop) and cost op (to get loss value)
      tmp <- sess$run(c(optimizer, cost), feed_dict = dict(X = batch_xs, Y = batch_ys))
      
      # Compute average loss
      avg_cost <- avg_cost + (tmp[[2]] / total_batch)
    }
    # Display logs per epoch step
    if (epoch %% display_step == 0){
      print(paste("Epoch: ", epoch, " Cost=", avg_cost))
    }
  }
  print("Optimization Finished!")
  
  # Test model
  correct_prediction <- tf$equal(tf$argmax(pred, 1L), tf$argmax(Y, 1L))
  # Calculate accuracy
  accuracy <- tf$reduce_mean(tf$cast(correct_prediction, tf$float32))
  print(paste("Test Accuracy: ", accuracy$eval(dict(X = TestX, Y = TestY_))))
})
