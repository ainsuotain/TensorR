# Auto Encoder
# implementation using tensorflow
# Build a 2 layers auto-encoder to compress images to
# a lower latent space and then reconstruct them.
# dataset : MNIST

library(tensorflow)
use_python("/home/spark/hard/anaconda3/bin/python3")
library(spatstat)
library(reticulate)

np <- import("numpy", convert = F)

# Import MNIST data
mnist_data <- py_run_string("
from tensorflow.examples.tutorials.mnist import input_data
mnist = input_data.read_data_sets('/tmp/data/', one_hot = False)
")

mnist <- mnist_data$mnist

# Training Parameters
learning_rate <- 0.01
num_steps <- 30000
batch_size <- 256

display_step <- 1000
examples_to_show <- 10

# Network Parameters
num_hidden_1 <- 256L
num_hidden_2 <- 128L
num_input <- 784L

# tf graph input (only pictures)
X <- tf$placeholder("float", shape = shape(NULL, num_input))

weights <- 
  dict(
    encoder_h1 = tf$Variable(tf$random_normal(shape(num_input, num_hidden_1))),
    encoder_h2 = tf$Variable(tf$random_normal(shape(num_hidden_1, num_hidden_2))),
    decoder_h1 = tf$Variable(tf$random_normal(shape(num_hidden_2, num_hidden_1))),
    decoder_h2 = tf$Variable(tf$random_normal(shape(num_hidden_1, num_input)))
  )

biases <- 
  dict(
    encoder_b1 = tf$Variable(tf$random_normal(shape(num_hidden_1))),
    encoder_b2 = tf$Variable(tf$random_normal(shape(num_hidden_2))),
    decoder_b1 = tf$Variable(tf$random_normal(shape(num_hidden_1))),
    decoder_b2 = tf$Variable(tf$random_normal(shape(num_input)))
  )

# Building the encoder
encoder <- function(x){
  # Encoder Hidden layer with sigmoid activation #1
  layer_1 <- tf$nn$sigmoid(tf$add(tf$matmul(x, weights$encoder_h1), biases$encoder_b1))
  # Encoder Hidden layer with sigmoid activation #2
  layer_2 <- tf$nn$sigmoid(tf$add(tf$matmul(layer_1, weights$encoder_h2), biases$encoder_b2))
  return(layer_2)
}

# Building the decoder
decoder <- function(x){
  # Encoder Hidden layer with sigmoid activation #1
  layer_1 <- tf$nn$sigmoid(tf$add(tf$matmul(x, weights$decoder_h1), biases$decoder_b1))
  # Encoder Hidden layer with sigmoid activation #2
  layer_2 <- tf$nn$sigmoid(tf$add(tf$matmul(layer_1, weights$decoder_h2), biases$decoder_b2))
  return(layer_2)
}

# Construct model
encoder_op <- encoder(X)
decoder_op <- decoder(encoder_op)

# Prediction
y_pred <- decoder_op
# Targets (Labels) are the input data.
y_true <- X

# Define loss and optimizer, minimize the squared error
loss <- tf$reduce_mean(tf$pow(y_true - y_pred, 2))
optimizer <- tf$train$RMSPropOptimizer(learning_rate = learning_rate)
train_op <- optimizer$minimize(loss)

# Initialize the variables (i.e. assign their default value)
init <- tf$global_variables_initializer()

# Start Training
with(tf$Session() %as% sess, {
  
  # Run the initializer
  sess$run(init)
  
  # Training
  for (i in 1:num_steps){
    
    # Prepare Data
    # Get the next batch of MNIST data (only images are needed, not labels)
    batch_data <- mnist$train$next_batch(as.integer(batch_size))
    batch_x <- batch_data[[1]]
    
    # Run optimization op (backprop) and cost op (to get loss value)
    tmp <- sess$run(c(train_op, loss), feed_dict = dict(X = batch_x))
    l <- tmp[[2]]
    # Display logs per step
    if (i %% display_step == 0 | i == 1){
      print(paste0("Step : ", i, ", Minibatch Loss : ", l))
    }
    
  }
  
  # Testing
  # Encode and decode images from test set and visualize their recontruction.
  n <- 1
  for (i in 1:n){
    # MNIST test set
    batch_test_x <- mnist$test$images[(4*(i-1)+1):(4*i),]
    g <- sess$run(decoder_op, feed_dict = dict(X = batch_test_x))
  }
  
})

# Visualization 
test_img <- im(matrix(batch_test_x[3,], nrow = 28L, byrow = T))
decode_img <- im(matrix(g[3,], nrow = 28L, byrow = T))

par(mfrow = c(1,2))
plot(test_img, main = paste0("True Test img"))
plot(decode_img, main = paste0("Decoded img"))




