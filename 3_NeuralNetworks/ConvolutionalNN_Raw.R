# Convolutional Neural Network
# implementation using tensorflow
# dataset : MNIST

library(tensorflow)
use_python("/home/spark/hard/anaconda3/bin/python3")

library(reticulate)

np <- import("numpy")

# Import MNIST data
mnist_data <- py_run_string("
from tensorflow.examples.tutorials.mnist import input_data
mnist = input_data.read_data_sets('/tmp/data/', one_hot = False)
")

mnist <- mnist_data$mnist

# Training Parameters
learning_rate <- 0.001
num_steps <- 1000L
batch_size <- 128L
display_step <- 10L

# Network Parameters
widths <- 28L # Widths of MNIST data input
heights <- 28L # Heights of MNIST data input
channel <- 1L # Channel of MNIST data input (gray scale)
num_classes <- 10L # MNIST classes (0 ~ 9 digits)
dropout <- 0.8 # Dropout, probability to keep units

tf$reset_default_graph()
# tf graph input
X <- tf$placeholder(tf$float32, shape = shape(NULL, as.integer(widths * heights * channel)))
Y <- tf$placeholder(tf$int32, shape = shape(NULL))
keep_prob <- tf$placeholder(tf$float32) # dropout (keep probability)

# Create some wrappers for simplicity
conv2d <- function(x, W, b, strides=1L){
  # Conv2D wrapper, wiht bias and relu activation
  x <- tf$nn$conv2d(x, W, strides = shape(1L, strides, strides, 1L), padding = "SAME")
  x <- tf$nn$bias_add(x, b)
  return(tf$nn$relu(x))
}

maxpool2d <- function(x, k=2L){
  # MaxPool2D wrapper
  return(tf$nn$max_pool(x, ksize = shape(1L, k, k, 1L), 
                        strides = shape(1L, k, k, 1L), padding = "SAME"))
}

# Create model
conv_net <- function(x, weight, biases, dropout){
  
  # reshape NX784 ==> NX28X28X1
  x <- tf$reshape(x, shape = shape(-1L, widths, heights, channel))
  
  # Convolution Layer
  conv1 <- conv2d(x, weight$wc1, biases$bc1)
  # Max Pooling (down-sampling)
  conv1 <- maxpool2d(conv1, k=2L)
  
  # Convolution Layer
  conv2 <- conv2d(conv1, weight$wc2, biases$bc2)
  # Max Pooling (down-sampling)
  conv2 <- maxpool2d(conv2, k=2L)
  
  # Fully connected layer
  # Reshape conv2 output to fit fully connected layer input
  
  shape_tmp <- weights_$wd1$get_shape()
  shape_tmp <- np$asarray(shape_tmp, dtype = np$int32)
  
  fc1 <- tf$reshape(conv2, shape = shape(-1L, shape_tmp[1]))
  fc1 <- tf$add(tf$matmul(fc1, weight$wd1), biases$bd1)
  fc1 <- tf$nn$relu(fc1)
  # Apply Dropout
  fc1 <- tf$nn$dropout(fc1, dropout)
  
  # Output, class prediction
  out <- tf$add(tf$matmul(fc1, weight$out), biases$out)
  return(out)
}

weights_ <- 
  dict(
    # 5X5 conv, 1 input, 32 outputs
    wc1 = tf$Variable(tf$random_normal(shape(5L, 5L, 1L, 32L))),
    # 5X5 conv, 32 input, 64 outputs
    wc2 = tf$Variable(tf$random_normal(shape(5L, 5L, 32L, 64L))),
    # fully connected, 7*7*64 inputs, 1024 outputs
    wd1 = tf$Variable(tf$random_normal(shape(7L*7L*64L, 1024L))),
    # 1024 inputs, 10 outputs (class prediction)
    out = tf$Variable(tf$random_normal(shape(1024L, num_classes)))
  )

biases_ <- 
  dict(
    bc1 = tf$Variable(tf$random_normal(shape(32L))),
    bc2 = tf$Variable(tf$random_normal(shape(64L))),
    bd1 = tf$Variable(tf$random_normal(shape(1024L))),
    out = tf$Variable(tf$random_normal(shape(num_classes)))
  )


# Construct model
logits <- conv_net(X, weights_, biases_, keep_prob)
prediction <- tf$nn$softmax(logits)

# Define loss and optimizer
loss_op <- tf$reduce_mean(
  tf$nn$sparse_softmax_cross_entropy_with_logits(logits = logits,
                                                 labels = Y)
  )
optimizer <- tf$train$AdamOptimizer(learning_rate = learning_rate)
train_op <- optimizer$minimize(loss_op)

# Evaluate model
correct_pred <- tf$equal(tf$cast(tf$argmax(prediction, 1L), tf$int32), Y)
accuracy <- tf$reduce_mean(tf$cast(correct_pred, tf$float32))

# Initialize the variables (i.e. assign their default value)
init <- tf$global_variables_initializer()

# Start training
with(tf$Session() %as% sess, {
  
  # Run the initializer
  sess$run(init)
  
  
  for (step in 1:num_steps){
    batch_idx <- sample(1:dim(mnist$train$images)[1], size = batch_size)
    batch_x <- mnist$train$images[batch_idx,]
    batch_y <- mnist$train$labels[batch_idx]

    # Run optimization op (backprop)
    sess$run(train_op, feed_dict = dict(
      X = batch_x,
      Y = batch_y,
      keep_prob = dropout
    ))

    if (step %% display_step == 0 | step == 1){
      # Calculate batch loss and accuracy
      tmp <- sess$run(c(loss_op, accuracy),
                      feed_dict = dict(
                        X = batch_x,
                        Y = batch_y,
                        keep_prob = 1.0
                      ))
      loss <- tmp[[1]]
      acc <- tmp[[2]]

      print(paste0("Step ", step, ", Minibatch Loss=",
                   round(loss, digits = 4), ", Training Acc=", round(acc, digits = 3)))
    }
  }
  print("Optimization Finished!")
  
  # Calculate accuracy for 256 MNIST test images
  test_batch_idx <- sample(1:dim(mnist$test$images)[1], size = 256L)
  batch_test_x <- mnist$test$images[test_batch_idx, ]
  batch_test_y <- mnist$test$labels[test_batch_idx]
  
  print(paste0("Test Acc = ", sess$run(accuracy,
                                       feed_dict = dict(
                                         X = batch_test_x,
                                         Y = batch_test_y,
                                         keep_prob = 1.0
                                       ))))
})
