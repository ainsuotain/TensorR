# Generative Adversarial Networks (GAN).
# implementation using tensorflow
# dataset : MNIST

library(tensorflow)
use_python("/home/spark/hard/anaconda3/bin/python3")
library(reticulate)

# Import MNIST data
mnist_data <- py_run_string("
from tensorflow.examples.tutorials.mnist import input_data
mnist = input_data.read_data_sets('/tmp/data/', one_hot = True)
")

mnist <- mnist_data$mnist

# Training Params
num_steps <- 100000L
batch_size <- 128L
learning_rate <- 0.0002

# Network Params
image_dim <- 784L
gen_hidden_dim <- 256L
disc_hidden_dim <- 256L
noise_dim <- 100L # Noise data points (latent vector)

# A custom initialization (see Xavier Glorot init)
glorot_init <- function(shape){
  return(tf$random_normal(shape = shape, stddev = 1. / tf$sqrt(shape[[1]] / 2.)))
}

# Store layers weights & bias with xavier init
weights_ <- 
  dict(
    gen_hidden1 = tf$Variable(glorot_init(shape(noise_dim, gen_hidden_dim))),
    gen_out = tf$Variable(glorot_init(shape(gen_hidden_dim, image_dim))),
    disc_hidden1 = tf$Variable(glorot_init(shape(image_dim, disc_hidden_dim))),
    disc_out = tf$Variable(glorot_init(shape(disc_hidden_dim, 1L)))
  )

biases_ <- 
  dict(
    gen_hidden1 = tf$Variable(tf$zeros(shape(gen_hidden_dim))),
    gen_out = tf$Variable(tf$zeros(shape(image_dim))),
    disc_hidden1 = tf$Variable(tf$zeros(shape(disc_hidden_dim))),
    disc_out = tf$Variable(tf$zeros(shape(1L)))
  )

# Generator
generator <- function(x){
  hidden_layer <- tf$matmul(x, weights_$gen_hidden1)
  hidden_layer <- tf$add(hidden_layer, biases_$gen_hidden1)
  hidden_layer <- tf$nn$relu(hidden_layer)
  out_layer <- tf$matmul(hidden_layer, weights_$gen_out)
  out_layer <- tf$add(out_layer, biases_$gen_out)
  out_layer <- tf$sigmoid(out_layer)
  return(out_layer)
}

# Discriminator
discriminator <- function(x){
  hidden_layer <- tf$matmul(x, weights_$disc_hidden1)
  hidden_layer <- tf$add(hidden_layer, biases_$disc_hidden1)
  hidden_layer <- tf$nn$relu(hidden_layer)
  out_layer <- tf$matmul(hidden_layer, weights_$disc_out)
  out_layer <- tf$add(out_layer, biases_$disc_out)
  out_layer <- tf$sigmoid(out_layer)
  return(out_layer)
}

# Build Networks
# Network Inputs
gen_input <- tf$placeholder(tf$float32, shape = shape(NULL, noise_dim), name = "input_noise")
disc_input <- tf$placeholder(tf$float32, shape = shape(NULL, image_dim), name = "disc_input")

# Build Generator Network
gen_sample <- generator(gen_input)

# Build 2 Discriminator Networks (one from noise input, one from generated samples)
disc_real <- discriminator(disc_input)
disc_fake <- discriminator(gen_sample)

# Build Loss
gen_loss <- -1 * tf$reduce_mean(tf$log(disc_fake))
disc_loss <- -1 * tf$reduce_mean(tf$log(disc_real) + tf$log(1. - disc_fake))

# Build Optimizers
optimizer_gen <- tf$train$AdamOptimizer(learning_rate = learning_rate)
optimizer_disc <- tf$train$AdamOptimizer(learning_rate = learning_rate)

# Training Variables for each optimizer
# By default in Tensorflow, all variables are updated by each optimizer,
# so we need to precise for each one of them the specific variables to update.
# Generator Network Variables
gen_vars <- 
  c(weights_$gen_hidden1, weights_$gen_out,
    biases_$gen_hidden1, biases_$gen_out)
# Discriminator Network Variables
disc_vars <- 
  c(weights_$disc_hidden1, weights_$disc_out,
    biases_$disc_hidden1, biases_$disc_out)

# Create training operations
train_gen <- optimizer_gen$minimize(gen_loss, var_list = gen_vars)
train_disc <- optimizer_gen$minimize(disc_loss, var_list = disc_vars)

# Initialize the variables (i.e. assign their default value)
init <- tf$global_variables_initializer()

# Start training
with(tf$Session() %as% sess, {
  
  # Run the initializer
  sess$run(init)
  
  for (i in 1:num_steps){
    # Prepare Data
    # Get the next batch of MNIST data (only images are needed, not labels)
    batch_data <- mnist$train$next_batch(batch_size)
    batch_x <- batch_data[[1]]
    # Generate noise to feed to the generator
    z <- matrix(runif(n = as.integer(batch_size * noise_dim), min = -1., max = 1.), 
                nrow = batch_size)
    
    # Train
    feed_dict <- 
      dict(
        disc_input = batch_x,
        gen_input = z
      )
    tmp <- sess$run(c(train_gen, train_disc, gen_loss, disc_loss),
                    feed_dict = feed_dict)
    gl <- tmp[[3]]
    dl <- tmp[[4]]
    
    if (i %% 1000 == 0 | i == 1){
      print(paste0("Step ", i, ": Generator Loss = ", round(gl, 5L), ", Discriminator Loss = ", round(dl, 5L)))
    }
  }
  
  # Generate image from noise, using the generator network.
  z <- matrix(runif(n = as.integer(4L * noise_dim), min = -1., max = 1.), 
              nrow = 4L)
  g <- sess$run(gen_sample, feed_dict = dict(
    gen_input = z
  ))
  
  gg <- array(g, dim = c(4L, 28L, 28L, 1L))
})

image(gg[1,,,])

