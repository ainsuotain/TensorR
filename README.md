## Tensorflow in R


## Folder Description

 - 1_introduction : 
    - 기본적인 Operation들을 사용하는 스크립트가 있는 폴더입니다. 
    - 이건 간단해서 하나의 스크립트로만 만들어두었습니다.

 - 2_BasicModels : 
    - 기본적인 머신러닝 알고리즘을 Tensorflow로 만들어봤습니다. 
    - 5개 알고리즘에 대해서 만들었는데 Kmeans와 RandomForest는 아예 tensorflow내에서도 모듈로 제공을 해줘서 쉽게 사용해볼수 있었습니다. 
    - 근데 h2o랑 비교했을땐 굳이 써야하나 싶긴하더군요. 
    - 나머지 3개는 low-level로 만들었습니다.

 - 3_NeuralNetworks : 
    - 기본적인 뉴럴넷부터 최근 각광받고있는 GAN까지 퀵하게 만들었습니다. 
    - 그리고 tensorflow의 estimator를 사용하는 코드도 넣어두었습니다. 
    - 아마 이 tf estimator가 정말 중요해질거같습니다.

 - Add_RLinR : 
    - 위 3개의 폴더는 다른 github자료를 참고하여 R에서 만든거지만 RL의 경우 제가 직접 구현하였습니다. 
    - Model free인데다가 pytorch로도 구현했던 적이 있어서 금방 구현할수 있었습니다. 
    - Deep Q Network, Policy Gradient 그리고 Advantage Actor Critic을 구현했으며, 환경은 openai gym의 cartpole로 셋팅했습니다.

## 주의사항
 - Rstudio에 가져가서 그냥 돌리시면 됩니다.
    - (물론, tensorflow와 reticulate는 기본적으로 설치가 되어있어야합니다.)
 - 단, import 함수로 불러오는 모듈들은 파이썬에 이미 설치가 되어있어야합니다.


## Reference :


    1) https://github.com/aymericdamien/TensorFlow-Examples/tree/master/examples
    
    2) https://tensorflow.rstudio.com/tfestimators